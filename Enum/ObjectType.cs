﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeApp.Enum
{
    public enum ObjectType
    {
        Window = 1,
        SubWindow = 2,
        Popup = 3,
    }
}
