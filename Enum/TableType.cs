﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeApp.Enum
{
    public enum TableType
    {
        AccountType = 1,
        Accounts = 2,
        Categories = 3,
        Products = 4,
        Bills = 5,
    }
}
