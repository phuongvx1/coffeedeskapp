﻿using CoffeeApp.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffeeApp
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private string email = "1";
        private string password = "1";


        private bool CheckLogin(string email,string password)
        {
            var check = false;

            if(this.email.Equals(email) && this.password.Equals(password))
            {
                check = true;
            }
            else
            {
                check = false;
            }

            return check;
        }

        private void BtnLoginClick(object sender, EventArgs e)
        {
            var inputEmail = tbEmail.Text;
            var inputPassword = tbPassword.Text;

            if (CheckLogin(inputEmail, inputPassword))
            {
                AdminView layout = new AdminView();
                layout.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Check email and password.");
            }
        }
    }
}
