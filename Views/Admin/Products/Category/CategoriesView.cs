﻿using CoffeeApp.Views.Admin.Products.Product;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffeeApp.Views.Admin.Products
{
    public partial class CategoriesView : UserControl
    {
        public CategoriesView()
        {
            InitializeComponent();
            pCategoriesAddNew.BringToFront();
        }

        private void CategoriesViewAllClick(object sender, EventArgs e)
        {
            pCategoriesViewAll.BringToFront();
        }

        private void CategoriesAddNewClick(object sender, EventArgs e)
        {
            pCategoriesAddNew.BringToFront();
        }
    }
}
