﻿namespace CoffeeApp.Views.Admin.Products
{
    partial class CategoriesView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbCategoriesViewAll = new System.Windows.Forms.Label();
            this.lbCategoriesAddNew = new System.Windows.Forms.Label();
            this.pCategoriesViewAll = new CoffeeApp.Views.Admin.Products.Category.CategoriesViewAll();
            this.pCategoriesAddNew = new CoffeeApp.Views.Admin.Products.Category.CategoriesAddNew();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbCategoriesViewAll);
            this.panel1.Controls.Add(this.lbCategoriesAddNew);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(924, 100);
            this.panel1.TabIndex = 0;
            // 
            // lbCategoriesView
            // 
            this.lbCategoriesViewAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCategoriesViewAll.Location = new System.Drawing.Point(173, 29);
            this.lbCategoriesViewAll.Name = "lbCategoriesView";
            this.lbCategoriesViewAll.Size = new System.Drawing.Size(136, 42);
            this.lbCategoriesViewAll.TabIndex = 1;
            this.lbCategoriesViewAll.Text = "View";
            this.lbCategoriesViewAll.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbCategoriesViewAll.Click += new System.EventHandler(this.CategoriesViewAllClick);
            // 
            // lbCategoriesNew
            // 
            this.lbCategoriesAddNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCategoriesAddNew.Location = new System.Drawing.Point(31, 29);
            this.lbCategoriesAddNew.Name = "lbCategoriesNew";
            this.lbCategoriesAddNew.Size = new System.Drawing.Size(136, 42);
            this.lbCategoriesAddNew.TabIndex = 0;
            this.lbCategoriesAddNew.Text = "New";
            this.lbCategoriesAddNew.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbCategoriesAddNew.Click += new System.EventHandler(this.CategoriesAddNewClick);
            // 
            // categoriesViewAll1
            // 
            this.pCategoriesViewAll.Location = new System.Drawing.Point(0, 106);
            this.pCategoriesViewAll.Name = "categoriesViewAll1";
            this.pCategoriesViewAll.Size = new System.Drawing.Size(921, 553);
            this.pCategoriesViewAll.TabIndex = 1;
            // 
            // categoriesAddNew1
            // 
            this.pCategoriesAddNew.Location = new System.Drawing.Point(0, 106);
            this.pCategoriesAddNew.Name = "categoriesAddNew1";
            this.pCategoriesAddNew.Size = new System.Drawing.Size(921, 540);
            this.pCategoriesAddNew.TabIndex = 2;
            // 
            // CategoriesView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pCategoriesAddNew);
            this.Controls.Add(this.pCategoriesViewAll);
            this.Controls.Add(this.panel1);
            this.Name = "CategoriesView";
            this.Size = new System.Drawing.Size(924, 558);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbCategoriesViewAll;
        private System.Windows.Forms.Label lbCategoriesAddNew;
        private Category.CategoriesViewAll pCategoriesViewAll;
        private Category.CategoriesAddNew pCategoriesAddNew;
    }
}
