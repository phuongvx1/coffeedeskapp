﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffeeApp.Views
{
    public partial class Products : UserControl
    {
        public Products()
        {
            InitializeComponent();
            pProductView.BringToFront();
        }

        private void ProductsClick(object sender, EventArgs e)
        {
            pProductView.BringToFront();
        }

        private void CategoriesClick(object sender, EventArgs e)
        {
            pCategoryView.BringToFront();
        }
    }
}
