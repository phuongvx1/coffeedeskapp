﻿namespace CoffeeApp.Views.Admin.Products
{
    partial class ProductsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbProductsViewAll = new System.Windows.Forms.Label();
            this.lbProductsAddNew = new System.Windows.Forms.Label();
            this.pProductViewAll = new CoffeeApp.Views.Admin.Products.Product.ProductsViewAll();
            this.pProductsAddNew = new CoffeeApp.Views.Admin.Products.Product.ProductsAddNew();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbProductsViewAll);
            this.panel1.Controls.Add(this.lbProductsAddNew);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(901, 100);
            this.panel1.TabIndex = 1;
            // 
            // lbProductsViewAll
            // 
            this.lbProductsViewAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbProductsViewAll.Location = new System.Drawing.Point(173, 29);
            this.lbProductsViewAll.Name = "lbProductsViewAll";
            this.lbProductsViewAll.Size = new System.Drawing.Size(136, 42);
            this.lbProductsViewAll.TabIndex = 1;
            this.lbProductsViewAll.Text = "View";
            this.lbProductsViewAll.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbProductsViewAll.Click += new System.EventHandler(this.ProductsViewAllClick);
            // 
            // lbProductsAddNew
            // 
            this.lbProductsAddNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbProductsAddNew.Location = new System.Drawing.Point(31, 29);
            this.lbProductsAddNew.Name = "lbProductsAddNew";
            this.lbProductsAddNew.Size = new System.Drawing.Size(136, 42);
            this.lbProductsAddNew.TabIndex = 0;
            this.lbProductsAddNew.Text = "New";
            this.lbProductsAddNew.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbProductsAddNew.Click += new System.EventHandler(this.ProductsAddNewClick);
            // 
            // pProductViewAll
            // 
            this.pProductViewAll.Location = new System.Drawing.Point(0, 97);
            this.pProductViewAll.Name = "pProductViewAll";
            this.pProductViewAll.Size = new System.Drawing.Size(901, 548);
            this.pProductViewAll.TabIndex = 2;
            // 
            // pProductsAddNew
            // 
            this.pProductsAddNew.Location = new System.Drawing.Point(0, 97);
            this.pProductsAddNew.Name = "pProductsAddNew";
            this.pProductsAddNew.Size = new System.Drawing.Size(903, 442);
            this.pProductsAddNew.TabIndex = 0;
            // 
            // ProductsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pProductsAddNew);
            this.Controls.Add(this.pProductViewAll);
            this.Controls.Add(this.panel1);
            this.Name = "ProductsView";
            this.Size = new System.Drawing.Size(901, 546);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbProductsViewAll;
        private System.Windows.Forms.Label lbProductsAddNew;
        private Product.ProductsViewAll pProductViewAll;
        private Product.ProductsAddNew pProductsAddNew;
    }
}
