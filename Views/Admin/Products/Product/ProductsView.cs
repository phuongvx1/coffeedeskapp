﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffeeApp.Views.Admin.Products
{
    public partial class ProductsView : UserControl
    {
        public ProductsView()
        {
            InitializeComponent();
            pProductsAddNew.BringToFront();
        }

        private void ProductsViewAllClick(object sender, EventArgs e)
        {
            pProductViewAll.BringToFront();
        }

        private void ProductsAddNewClick(object sender, EventArgs e)
        {
            pProductsAddNew.BringToFront();
        }
    }
}
