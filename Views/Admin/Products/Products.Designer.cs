﻿namespace CoffeeApp.Views
{
    partial class Products
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pMenuProducts = new System.Windows.Forms.Panel();
            this.lbCategories = new System.Windows.Forms.Label();
            this.lbProducts = new System.Windows.Forms.Label();
            this.pProductView = new CoffeeApp.Views.Admin.Products.ProductsView();
            this.pCategoryView = new CoffeeApp.Views.Admin.Products.CategoriesView();
            this.pMenuProducts.SuspendLayout();
            this.SuspendLayout();
            // 
            // pMenuProducts
            // 
            this.pMenuProducts.Controls.Add(this.lbCategories);
            this.pMenuProducts.Controls.Add(this.lbProducts);
            this.pMenuProducts.Dock = System.Windows.Forms.DockStyle.Top;
            this.pMenuProducts.Location = new System.Drawing.Point(0, 0);
            this.pMenuProducts.Name = "pMenuProducts";
            this.pMenuProducts.Size = new System.Drawing.Size(1650, 100);
            this.pMenuProducts.TabIndex = 1;
            // 
            // lbCategories
            // 
            this.lbCategories.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCategories.Location = new System.Drawing.Point(197, 30);
            this.lbCategories.Name = "lbCategories";
            this.lbCategories.Size = new System.Drawing.Size(170, 40);
            this.lbCategories.TabIndex = 4;
            this.lbCategories.Text = "Categories";
            this.lbCategories.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbCategories.Click += new System.EventHandler(this.CategoriesClick);
            // 
            // lbProducts
            // 
            this.lbProducts.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbProducts.Location = new System.Drawing.Point(21, 30);
            this.lbProducts.Name = "lbProducts";
            this.lbProducts.Size = new System.Drawing.Size(170, 40);
            this.lbProducts.TabIndex = 0;
            this.lbProducts.Text = "Products";
            this.lbProducts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbProducts.Click += new System.EventHandler(this.ProductsClick);
            // 
            // pProductView
            // 
            this.pProductView.Location = new System.Drawing.Point(0, 100);
            this.pProductView.Name = "pProductView";
            this.pProductView.Size = new System.Drawing.Size(1650, 980);
            this.pProductView.TabIndex = 2;
            // 
            // pCategoryView
            // 
            this.pCategoryView.Location = new System.Drawing.Point(0, 100);
            this.pCategoryView.Name = "pCategoryView";
            this.pCategoryView.Size = new System.Drawing.Size(1650, 980);
            this.pCategoryView.TabIndex = 3;
            // 
            // Products
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pCategoryView);
            this.Controls.Add(this.pProductView);
            this.Controls.Add(this.pMenuProducts);
            this.Name = "Products";
            this.Size = new System.Drawing.Size(1650, 1080);
            this.pMenuProducts.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pMenuProducts;
        private System.Windows.Forms.Label lbProducts;
        private System.Windows.Forms.Label lbCategories;
        private Admin.Products.ProductsView pProductView;
        private Admin.Products.CategoriesView pCategoryView;
    }
}
