﻿namespace CoffeeApp.Views
{
    partial class AdminView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.paneName = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDashBoard = new System.Windows.Forms.Label();
            this.lblProducts = new System.Windows.Forms.Label();
            this.lblBills = new System.Windows.Forms.Label();
            this.paneMenu = new System.Windows.Forms.Panel();
            this.paneDashBoard = new CoffeeApp.Views.DashBoard();
            this.paneBills = new CoffeeApp.Views.Bills();
            this.paneProducts = new CoffeeApp.Views.Products();
            this.paneName.SuspendLayout();
            this.paneMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // paneName
            // 
            this.paneName.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.paneName.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.paneName.Controls.Add(this.label1);
            this.paneName.Location = new System.Drawing.Point(0, 0);
            this.paneName.Name = "paneName";
            this.paneName.Size = new System.Drawing.Size(400, 140);
            this.paneName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(0, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(224, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "VNHP-Coffee";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDashBoard
            // 
            this.lblDashBoard.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblDashBoard.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDashBoard.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblDashBoard.Location = new System.Drawing.Point(0, 140);
            this.lblDashBoard.Name = "lblDashBoard";
            this.lblDashBoard.Size = new System.Drawing.Size(400, 70);
            this.lblDashBoard.TabIndex = 1;
            this.lblDashBoard.Text = "DashBoard";
            this.lblDashBoard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDashBoard.Click += new System.EventHandler(this.DashBoardClick);
            // 
            // lblProducts
            // 
            this.lblProducts.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProducts.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblProducts.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblProducts.Location = new System.Drawing.Point(0, 210);
            this.lblProducts.Name = "lblProducts";
            this.lblProducts.Size = new System.Drawing.Size(400, 70);
            this.lblProducts.TabIndex = 2;
            this.lblProducts.Text = "Products";
            this.lblProducts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblProducts.Click += new System.EventHandler(this.ProductsClick);
            // 
            // lblBills
            // 
            this.lblBills.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBills.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblBills.Location = new System.Drawing.Point(0, 280);
            this.lblBills.Name = "lblBills";
            this.lblBills.Size = new System.Drawing.Size(400, 70);
            this.lblBills.TabIndex = 5;
            this.lblBills.Text = "Bills";
            this.lblBills.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblBills.Click += new System.EventHandler(this.BillsClick);
            // 
            // paneMenu
            // 
            this.paneMenu.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.paneMenu.Controls.Add(this.lblBills);
            this.paneMenu.Controls.Add(this.lblProducts);
            this.paneMenu.Controls.Add(this.lblDashBoard);
            this.paneMenu.Controls.Add(this.paneName);
            this.paneMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.paneMenu.Location = new System.Drawing.Point(0, 0);
            this.paneMenu.Name = "paneMenu";
            this.paneMenu.Size = new System.Drawing.Size(400, 1055);
            this.paneMenu.TabIndex = 1;
            // 
            // paneDashBoard
            // 
            this.paneDashBoard.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.paneDashBoard.Location = new System.Drawing.Point(400, 0);
            this.paneDashBoard.Name = "paneDashBoard";
            this.paneDashBoard.Size = new System.Drawing.Size(1650, 1080);
            this.paneDashBoard.TabIndex = 2;
            // 
            // paneBills
            // 
            this.paneBills.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.paneBills.Location = new System.Drawing.Point(400, 0);
            this.paneBills.Name = "paneBills";
            this.paneBills.Size = new System.Drawing.Size(1650, 1080);
            this.paneBills.TabIndex = 4;
            // 
            // paneProducts
            // 
            this.paneProducts.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.paneProducts.Location = new System.Drawing.Point(400, 0);
            this.paneProducts.Name = "paneProducts";
            this.paneProducts.Size = new System.Drawing.Size(1650, 1080);
            this.paneProducts.TabIndex = 5;
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1924, 1055);
            this.Controls.Add(this.paneProducts);
            this.Controls.Add(this.paneBills);
            this.Controls.Add(this.paneDashBoard);
            this.Controls.Add(this.paneMenu);
            this.MaximizeBox = false;
            this.Name = "Admin";
            this.Text = "Coffee Shop";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Layout_Load);
            this.paneName.ResumeLayout(false);
            this.paneName.PerformLayout();
            this.paneMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel paneName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDashBoard;
        private System.Windows.Forms.Label lblProducts;
        private System.Windows.Forms.Label lblBills;
        private System.Windows.Forms.Panel paneMenu;
        private DashBoard paneDashBoard;
        private Bills paneBills;
        private Products paneProducts;
    }
}
