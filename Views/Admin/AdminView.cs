﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffeeApp.Views
{
    public partial class AdminView : Form
    {
        public AdminView()
        {
            InitializeComponent();
            paneName.BackColor = Color.FromArgb(30, 30, 30);
            paneName.Width = 370;

            paneMenu.Width = 370;

            OnHoverLabel(lblDashBoard);
            OnHoverLabel(lblProducts);
            OnHoverLabel(lblBills);

            paneDashBoard.BringToFront();
        }

        private string GetLabelName(string sender)
        {
            var startIndex = sender.LastIndexOf(" ");
            var lengthName = sender.Length - sender.LastIndexOf(" ");
            var labelName = sender.Substring(startIndex, lengthName);
            return labelName;
        }

        private Label GetLabel(string name)
        {
            Label label = new Label();

            return label;
        }

        private void OnMouseEnter(object sender, EventArgs e)
        {
            switch (GetLabelName(sender.ToString()).TrimStart())
            {
                case "DashBoard":
                    lblDashBoard.BackColor = Color.FromArgb(30, 30, 30);
                    break;
                case "Bills":
                    lblBills.BackColor = Color.FromArgb(30, 30, 30);
                    break;
                case "Products":
                    lblProducts.BackColor = Color.FromArgb(30, 30, 30);
                    break;
                default:
                    break;
            }
        }

        private void OnMouseLeave(object sender, EventArgs e)
        {
            switch (GetLabelName(sender.ToString()).TrimStart())
            {
                case "DashBoard":
                    lblDashBoard.BackColor = Color.FromArgb(0, 0, 0);
                    break;
                case "Bills":
                    lblBills.BackColor = Color.FromArgb(0, 0, 0);
                    break;
                case "Products":
                    lblProducts.BackColor = Color.FromArgb(0, 0, 0);
                    break;
            }
        }

        private void OnHoverLabel(Label label)
        {
            label.MouseEnter += OnMouseEnter;
            label.MouseLeave += OnMouseLeave;
        }

        private void DashBoardClick(object sender, EventArgs e)
        {
            paneDashBoard.BringToFront();
        }

        private void ProductsClick(object sender, EventArgs e)
        {
            paneProducts.BringToFront();
        }

        private void BillsClick(object sender, EventArgs e)
        {
            paneBills.BringToFront();
        }

        private void Layout_Load(object sender, EventArgs e)
        {

        }
    }
}
